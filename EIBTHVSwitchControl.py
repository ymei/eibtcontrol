#!/usr/bin/env python

import Tkinter as tk
import SimPlot
import threading
import math,sys,time
import NIDAQmxIO

class CommonData:

    def __init__(self):
        # number of voltages to control
        self.nVolts = 5
        # update time interval (second)
        self.tI = 0.5
        # number of points to plot
        self.nPP = 120

        self.cv = threading.Condition() # condition variable
        self.quit = False
        self.isRamping = False

        self.voltsUpperValues = [0.0 for i in xrange(self.nVolts)]
        self.voltsRampRateValues = [0.0 for i in xrange(self.nVolts)]
        self.voltsRampStopValues = [0 for i in xrange(self.nVolts)]
        self.voltsOutput = [0.0 for i in xrange(self.nVolts)]

        self.inputVs = [0.0 for i in xrange(self.nVolts)]
        self.inputIs = [0.0 for i in xrange(self.nVolts)]

class ControlPanelGUI:

    def __init__(self, master, cd):
        self.master = master
        self.cd = cd
        self.nVolts = cd.nVolts
        # appropriate quitting
        master.wm_protocol("WM_DELETE_WINDOW", self.quit)
        # frame for controls
        self.voltagesFrame = tk.Frame(master)
        self.voltagesFrame.pack(side=tk.TOP)
        # frame for plotting
        self.graphFrame = tk.Frame(master)
        self.graphFrame.pack(side=tk.BOTTOM)
        self.plt = SimPlot.SimPlot(self.graphFrame)
        self.pltGraph = self.plt.makeGraphBase(self.graphFrame, 600, 400, title='Voltage Readback',
                                               xtitle = 'point #', ytitle = 'V')
        self.pltGraph.pack(fill=tk.BOTH, expand=tk.YES)
        # variables for plotting    
        self.pltData = [[(float(i), 0.0) for i in xrange(cd.nPP)] for j in xrange(self.nVolts)]
        self.pltI = 0 # current filling index

        # GUI widgets    
        self.voltsLabels =  [tk.Label(self.voltagesFrame, text="%d" % (i+1))
                             for i in xrange(self.nVolts)]
        self.voltsVLabels = [tk.Label(self.voltagesFrame, font="Courier 10", text="0.0 V")
                             for i in xrange(self.nVolts)]
        self.voltsILabels = [tk.Label(self.voltagesFrame, font="Courier 10", text="0.0 A")
                             for i in xrange(self.nVolts)]

        self.voltsUpperVars = [tk.DoubleVar() for i in xrange(self.nVolts)]
        self.voltsUpperEntries = [tk.Entry(self.voltagesFrame, width=8, justify=tk.RIGHT,
                                           textvariable=self.voltsUpperVars[i])
                                           for i in xrange(self.nVolts)]

        self.voltsRampRateVars = [tk.DoubleVar() for i in xrange(self.nVolts)]
        self.voltsRampRateEntries = [tk.Entry(self.voltagesFrame, width=8, justify=tk.RIGHT,
                                              textvariable=self.voltsRampRateVars[i])
                                              for i in xrange(self.nVolts)]

        self.voltsRampStopVars = [tk.IntVar() for i in xrange(self.nVolts)]
        self.voltsRampStopRadios = [
            (tk.Radiobutton(self.voltagesFrame, variable=self.voltsRampStopVars[i], value=0,
                            command=self.volts_ramp_stop_radios_sel),
             tk.Radiobutton(self.voltagesFrame, variable=self.voltsRampStopVars[i], value=1,
                            command=self.volts_ramp_stop_radios_sel)) for i in xrange(self.nVolts)
        ]

        self.voltsOutputLabels = [tk.Label(self.voltagesFrame, font="Courier 10", text="0.0 V")
                                  for i in xrange(self.nVolts)]

        # caption    
        tk.Label(self.voltagesFrame, text="HV", fg="white", bg="black").grid(row=0, column=0)
        tk.Label(self.voltagesFrame, text="Voltage", width=10,
                 fg="white", bg="black").grid(row=0, column=1)
        tk.Label(self.voltagesFrame, text="Current", width=10,
                 fg="white", bg="black").grid(row=0, column=2)
        tk.Label(self.voltagesFrame, text="Set Point (V)",
                 fg="white", bg="black").grid(row=0, column=3)
        tk.Label(self.voltagesFrame, text="Ramp Rate (V/s)",
                 fg="white", bg="black").grid(row=0, column=4)
        tk.Label(self.voltagesFrame, text="Stop", fg="white", bg="black").grid(row=0, column=5)
        tk.Label(self.voltagesFrame, text="Ramp", fg="white", bg="black").grid(row=0, column=6)
        tk.Label(self.voltagesFrame, text="Vout", width=10,
                 fg="white", bg="black").grid(row=0, column=7)

        # placing widgets        
        for i in xrange(self.nVolts):
            self.voltsLabels[i].grid(row=i+1,column=0)
            self.voltsVLabels[i].grid(row=i+1, column=1)
            self.voltsILabels[i].grid(row=i+1, column=2)
            self.voltsUpperEntries[i].grid(row=i+1, column=3)
            self.voltsRampRateEntries[i].grid(row=i+1, column=4)
            self.voltsRampStopRadios[i][0].grid(row=i+1, column=5)
            self.voltsRampStopRadios[i][1].grid(row=i+1, column=6)
            self.voltsOutputLabels[i].grid(row=i+1, column=7)
            
        # self-updating functions
        self.update_values_display()
        self.update_graph()

    def volts_ramp_stop_radios_sel(self):
        with self.cd.cv:
            self.cd.isRamping = self.is_ramping()
            for i in xrange(self.nVolts):
                self.cd.voltsUpperValues[i] = self.voltsUpperVars[i].get()
                self.cd.voltsRampRateValues[i] = self.voltsRampRateVars[i].get()
            print "Set Point: ",
            print self.cd.voltsUpperValues
            print "Ramp Rate: ",
            print self.cd.voltsRampRateValues
            self.cd.cv.notify()
        return True

    def is_ramping(self):
        f = 0
        for i in xrange(self.nVolts):
            self.cd.voltsRampStopValues[i] = self.voltsRampStopVars[i].get()
            f += self.cd.voltsRampStopValues[i]
        return (f>0)

    def update_values_display(self):
        for i in xrange(self.nVolts):
            self.voltsVLabels[i].configure(text="% 7.3f" % self.cd.inputVs[i])
            self.voltsILabels[i].configure(text="% 7.3f" % self.cd.inputIs[i])
            self.voltsOutputLabels[i].configure(text="% 7.3f" % self.cd.voltsOutput[i])
        self.master.after(int(1000*self.cd.tI), self.update_values_display)

    def update_graph(self):
        for i in xrange(self.nVolts):
            self.pltData[i][self.pltI] = (float(self.pltI), self.cd.inputVs[i])
        self.pltI += 1
        if self.pltI >= self.cd.nPP: self.pltI = 0
        colors = ['red', 'green', 'blue', 'cyan', 'black']
        lines = []
        for i in xrange(self.nVolts):
            lines.append(self.plt.makeLine(self.pltData[i], color=colors[i], width=2, smooth=0))
        graphObject = self.plt.makeGraphObjects(lines)
        self.pltGraph.clear()
        self.pltGraph.draw(graphObject, xaxis = 'minimal', yaxis = 'minimal')
        self.master.after(int(1000*self.cd.tI), self.update_graph)

    def quit(self):
        with self.cd.cv:
            self.cd.quit = True
            self.cd.cv.notify()
        self.master.destroy()

class HVControl(threading.Thread):
    # do not try to access tk.IntVar etc. here.  Since after
    # master.destroy(), those variables associated with tk seem to be
    # destroyed as well and accessing them would result this thread to
    # hang.
    
    def __init__(self, cd, nis):
        threading.Thread.__init__(self)
        self.cd = cd
        self.nis = nis
        self.t = 0.0

    def run(self):
        with self.cd.cv:
            while not self.cd.quit:
                self.cd.cv.wait(self.cd.tI)
                if self.cd.isRamping:
                    print "Ramping!"
                self.set_voltage_outputs()
                self.get_inputs()

    def set_voltage_outputs(self):
        for i in xrange(self.cd.nVolts):
            name = self.nis[1].physicalChannel[i]
            if self.cd.voltsRampStopValues[i] == 0: # stop
                self.nis[1].write(self.cd.voltsOutput[i], name)
            else: # ramping
                newval = self.cd.voltsOutput[i] + self.cd.tI * self.cd.voltsRampRateValues[i]
                if newval < self.cd.voltsUpperValues[i] and newval >= -sys.float_info.epsilon:
                    self.cd.voltsOutput[i] = newval
                self.nis[1].write(self.cd.voltsOutput[i], name)
        return
    def get_inputs(self):
        ret = self.nis[0].read_all()
        for i in xrange(self.cd.nVolts):
            name = self.nis[0].physicalChannel[i]
            self.cd.inputVs[i] = ret[name]
            name = self.nis[0].physicalChannel[i+self.cd.nVolts]
            self.cd.inputIs[i] = ret[name]
            # self.cd.inputVs[i] = math.sin(self.t)
            # self.t += 1.0
            # self.cd.inputIs[i] = math.sin(self.t)
            # self.t += 1.0
        return

root = tk.Tk()
root.wm_title("EIBT High Voltage Switch Control")
cd = CommonData()
controlPanel = ControlPanelGUI(root, cd)

input_channels = ["Dev3/ai0", "Dev3/ai1", "Dev3/ai7", "Dev3/ai3", "Dev3/ai4", # voltage inputs
                  "Dev3/ai5", "Dev3/ai6", "Dev3/ai2", "Dev3/ai8", "Dev3/ai9"] # current inputs
niIn = NIDAQmxIO.NIDAQmxIO(input_channels)
niIn.configure()
output_channels = ["Dev2/ao0", "Dev2/ao1", "Dev2/ao2", "Dev2/ao3", "Dev2/ao7"]
niOut = NIDAQmxIO.NIDAQmxIO(output_channels, "out")
niOut.configure()

hvControl = HVControl(cd,[niIn,niOut])
hvControl.start()
root.mainloop()
hvControl.join()
