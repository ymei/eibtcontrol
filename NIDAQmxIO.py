from PyDAQmx.DAQmxFunctions import *
from PyDAQmx.DAQmxConstants import *

class NIDAQmxIO():
    """ National Instruments (NI-DAQmx) wrapper class to provide I/O
    on selected channels.
    """
    def __init__(self, physicalChannel, direction="in", limit=None, reset=False):
        if type(physicalChannel) == type(""):
            self.physicalChannel = [physicalChannel]
        else:
            self.physicalChannel = physicalChannel
        self.numberOfChannel = physicalChannel.__len__()
        self.direction = direction
        if limit is None:
            self.limit = dict([(name, (-10.0,10.0)) for name in self.physicalChannel])
        elif type(limit) == tuple:
            self.limit = dict([(name, limit) for name in self.physicalChannel])
        else:
            self.limit = dict([(name, limit[i]) for  i,name in enumerate(self.physicalChannel)])           
        if reset:
            DAQmxResetDevice(physicalChannel[0].split('/')[0] )
    def configure(self):
        # Create one task handle per Channel
        taskHandles = dict([(name,TaskHandle(0)) for name in self.physicalChannel])
        for name in self.physicalChannel:
            DAQmxCreateTask("",byref(taskHandles[name]))
            if self.direction == "in":
                DAQmxCreateAIVoltageChan(taskHandles[name],name,"",DAQmx_Val_RSE,
                                         self.limit[name][0],self.limit[name][1],
                                         DAQmx_Val_Volts,None)
            else:
                DAQmxCreateAOVoltageChan(taskHandles[name],name,"",
                                         self.limit[name][0],self.limit[name][1],
                                         DAQmx_Val_Volts,None)
        self.taskHandles = taskHandles
    def read_all(self):
        return dict([(name,self.read(name)) for name in self.physicalChannel])
    def read(self,name = None):
        if name is None:
            name = self.physicalChannel[0]
        taskHandle = self.taskHandles[name]                    
        DAQmxStartTask(taskHandle)

        data = (float64)()
        read = int32()
        DAQmxReadAnalogF64(taskHandle,1,DAQmx_Val_WaitInfinitely,DAQmx_Val_GroupByChannel,
                           data,1,byref(read),None)
        DAQmxStopTask(taskHandle)
        return data.value
    def write(self,val,name):
        taskHandle = self.taskHandles[name]
        data = (float64)()
        data.value = float(val)
        write = int32()
        DAQmxStartTask(taskHandle)
        DAQmxWriteAnalogF64(taskHandle,1,1,DAQmx_Val_WaitInfinitely,DAQmx_Val_GroupByChannel,
                            data,byref(write),None)
        DAQmxStopTask(taskHandle)
        return write.value
